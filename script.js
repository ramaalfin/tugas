function toggle(){
    const toggle = document.querySelector(".toggle");
    const navigation = document.querySelector(".nav");
    toggle.classList.toggle('active');
    navigation.classList.toggle('active');
}